# singularity-prod

This repository produces singularity v3.4 images with yade pre-installed, based on https://gitlab.com/yade-dev/docker-prod.
